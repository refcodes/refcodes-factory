// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.util.Map;

import org.refcodes.exception.UnmarshalException;

/**
 * The {@link UnmarshalFactory} defines the functionality which must be provided
 * in order to represent a factory for object creation of an object being of a
 * requested type (in contrast to the {@link TypeFactory} and the
 * {@link LookupFactory}) from an instance of a provided type. Many alternative
 * implementations of a {@link UnmarshalFactory} may exist which construct the
 * instances their way. Having factories that generic as we define it here, we
 * are able to decouple our business logic from any specific framework: Your
 * business logic must not know anything about how the instances are generated.
 * It mainly just needs to know how to use the {@link UnmarshalFactory}. It is
 * up to the application "end point", i.e. a command line tool with a
 * main-method or a web-application to finally decide which factory to use.
 * <p>
 * Depending on the implementation used or configuration provided, the
 * {@link UnmarshalFactory} may return singletons or dedicated separate
 * instances when queried for instances.
 * 
 * @param <SRC> The context which may influence the object being created by this
 *        factory.
 */
@FunctionalInterface
public interface UnmarshalFactory<SRC> {

	/**
	 * This method creates / retrieves an instance of the given type from the
	 * provided instance.
	 *
	 * @param <T> The type which is to be fabricated by the factory method.
	 * @param aContext The context which may influence the object being created
	 *        by this factory.
	 * @param aType The type identifying the instance to be created / retrieved.
	 * 
	 * @return An instance of the required type for the given type and context.
	 * 
	 * @throws UnmarshalException Thrown when unmarshaling / deserializing an
	 *         object fails.
	 */
	<T> T toUnmarshaled( SRC aContext, Class<T> aType ) throws UnmarshalException;

	/**
	 * This method creates / retrieves an instance of one of the given types
	 * from the provided instance. The returned instance's type is to be tested
	 * e.g. via "instanceof".
	 *
	 * @param aContext The context which may influence the object being created
	 *        by this factory.
	 * @param aTypes The types identifying the instance to be created /
	 *        retrieved.
	 * 
	 * @return An instance of the type for which unmarshaling the context
	 *         succeeded. You have to verify yourself which type you actually
	 *         retrieved e.g. via "instanceof".
	 * 
	 * @throws UnmarshalException Thrown when unmarshaling / deserializing for
	 *         all provided types failed.
	 */
	default Object toUnmarshaled( SRC aContext, Class<?>... aTypes ) throws UnmarshalException {
		for ( Class<?> eType : aTypes ) {
			try {
				return toUnmarshaled( aContext, eType );
			}
			catch ( UnmarshalException ignore ) { /* ignore */ }
		}
		throw new UnmarshalException( "Unable to unmarshal the context of type <" + aContext.getClass().getName() + "> to fit into one of the provided types." );
	}

	/**
	 * This method creates / retrieves an instance of one of the given types
	 * from the provided instance. The returned instance's type is to be tested
	 * e.g. via "instanceof".
	 *
	 * @param aContext The context which may influence the object being created
	 *        by this factory.
	 * @param aProperties The dynamic properties which are used to configure the
	 *        desired bean.
	 * @param aTypes The types identifying the instance to be created /
	 *        retrieved.
	 * 
	 * @return An instance of the type for which unmarshaling the context
	 *         succeeded. You have to verify yourself which type you actually
	 *         retrieved e.g. via "instanceof".
	 * 
	 * @throws UnmarshalException Thrown when unmarshaling / deserializing for
	 *         all provided types failed.
	 */
	default Object toUnmarshaled( SRC aContext, Map<String, String> aProperties, Class<?>... aTypes ) throws UnmarshalException {
		for ( Class<?> eType : aTypes ) {
			try {
				return toUnmarshaled( aContext, aProperties, eType );
			}
			catch ( UnmarshalException ignore ) { /* ignore */ }
		}
		throw new UnmarshalException( "Unable to unmarshal the context of type <" + aContext.getClass().getName() + "> to fit into one of the provided types." );
	}

	/**
	 * This method creates / retrieves all instances of the given type.
	 * 
	 * @param <T> The type which is to be fabricated by the factory method.
	 * @param aContext The context which may influence the object being created
	 *        by this factory.
	 * @param aType The type identifying the instances to be created /
	 *        retrieved.
	 * @param aProperties The dynamic properties which are used to configure the
	 *        desired bean.
	 * 
	 * @return An instance of the required type for the given type and context.
	 * 
	 * @throws UnmarshalException Thrown when unmarshaling / deserializing an
	 *         object fails.
	 */
	default <T> T toUnmarshaled( SRC aContext, Class<T> aType, Map<String, String> aProperties ) throws UnmarshalException {
		return toUnmarshaled( aContext, aType );
	}

	/**
	 * The {@link UnmarshalFactoryComplement} provides inverse functionality to
	 * the {@link UnmarshalFactory}.
	 *
	 * @param <C> The type of the complement regarding the type of the
	 *        {@link UnmarshalFactory}.
	 */
	public interface UnmarshalFactoryComplement<C> {

		/**
		 * Same as {@link UnmarshalFactory#toUnmarshaled(Object, Class)} passing
		 * the (source's) complement instead of the source.
		 *
		 * @param <T> the generic type
		 * @param aContext The context influencing the creation of the instance.
		 * @param aType The type identifying the instance to be created /
		 *        retrieved.
		 * 
		 * @return The produced instance.
		 * 
		 * @throws UnmarshalException the unmarshal exception
		 */
		<T> T fromMarshaled( C aContext, Class<T> aType ) throws UnmarshalException;

		/**
		 * Same as {@link UnmarshalFactory#toUnmarshaled(Object, Class...)}
		 * passing the (source's) complement instead of the source.
		 *
		 * @param aContext The context influencing the creation of the instance.
		 * @param aTypes The types identifying or included by the instance to be
		 *        created / retrieved.
		 * 
		 * @return The produced instance.
		 * 
		 * @throws UnmarshalException the unmarshal exception
		 */
		Object fromMarshaled( C aContext, Class<?>... aTypes ) throws UnmarshalException;

		/**
		 * Same as {@link UnmarshalFactory#toUnmarshaled(Object, Map, Class...)}
		 * passing the (source's) complement instead of the source.
		 *
		 * @param aContext The context influencing the creation of the instance.
		 * @param aProperties the properties
		 * @param aTypes The types identifying or included by the instance to be
		 *        created / retrieved.
		 * 
		 * @return The produced instance.
		 * 
		 * @throws UnmarshalException the unmarshal exception
		 */
		Object fromMarshaled( C aContext, Map<String, String> aProperties, Class<?>... aTypes ) throws UnmarshalException;

		/**
		 * Same as {@link UnmarshalFactory#toUnmarshaled(Object, Class, Map)}
		 * passing the (source's) complement instead of the source.
		 *
		 * @param <T> the generic type
		 * @param aContext The context influencing the creation of the instance.
		 * @param aType The type identifying the instance to be created /
		 *        retrieved.
		 * @param aProperties the properties
		 * 
		 * @return The produced instance.
		 * 
		 * @throws UnmarshalException the unmarshal exception
		 */
		<T> T fromMarshaled( C aContext, Class<T> aType, Map<String, String> aProperties ) throws UnmarshalException;

	}

	/**
	 * The {@link UnmarshalFactoryComposite} provides {@link UnmarshalFactory}
	 * as well as {@link UnmarshalFactoryComplement} functionality.
	 *
	 * @param <T> The type of the instances being served by the
	 *        {@link UnmarshalFactory} factory.
	 * @param <C> The type of the complement regarding the type of the
	 *        {@link UnmarshalFactory}.
	 */
	public interface UnmarshalFactoryComposite<T, C> extends UnmarshalFactory<T>, UnmarshalFactoryComplement<C> {}

}
