// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.io.InputStream;
import java.util.Map;

import org.refcodes.exception.MarshalException;

/**
 * The {@link MarshalTypeFactory} defines the functionality which must be
 * provided in order to create marshaling functionality from an internal
 * representation of a given data structure to an external representation such
 * as a {@link String} or an {@link InputStream}.
 * 
 * @param <T> The type of the data structure from which a marshaled (external)
 *        representation is to be unmarshaled.
 * @param <SRC> The type of the marshaled (external) representation.
 */
@FunctionalInterface
public interface MarshalTypeFactory<T, SRC> {

	/**
	 * This method creates / retrieves (converts to) from the provided data
	 * structure to its external representation.
	 *
	 * @param aDataStructure The data structure to be marshaled.
	 * 
	 * @return The according marshaled data structure from the external
	 *         representation.
	 * 
	 * @throws MarshalException Thrown when marshaling / serializing a data
	 *         structure fails.
	 */
	SRC toMarshaled( T aDataStructure ) throws MarshalException;

	/**
	 * This method creates / retrieves (converts to) an instance from the
	 * provided external representation.
	 *
	 * @param aDataStructure The data structure to be marshaled.
	 * @param aProperties The dynamic properties which are used to configure the
	 *        desired result.
	 * 
	 * @return The according marshaled data structure from the external
	 *         representation.
	 * 
	 * @throws MarshalException Thrown when marshaling / serializing a data
	 *         structure fails.
	 */
	default SRC toMarshaled( T aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		return toMarshaled( aDataStructure );
	}

	/**
	 * The {@link MarshalTypeFactoryComplement} provides inverse functionality
	 * to the {@link MarshalTypeFactory}.
	 *
	 * @param <T> The type of the data structure from which a marshaled
	 *        (external) representation is to be unmarshaled.
	 * @param <C> The type of the complement regarding the type of the
	 *        unmarshaled representation of the {@link MarshalTypeFactory}.
	 */
	public interface MarshalTypeFactoryComplement<T, C> {

		/**
		 * This method creates / retrieves (converts to) an instance from the
		 * provided external representation.
		 *
		 * @param aDataStructure The data structure to be marshaled.
		 * @param aProperties The dynamic properties which are used to configure
		 *        the desired result.
		 * 
		 * @return The according marshaled data structure from the external
		 *         representation.
		 * 
		 * @throws MarshalException Thrown when marshaling / serializing a data
		 *         structure fails.
		 */
		default C fromUnmarshaled( T aDataStructure, Map<String, String> aProperties ) throws MarshalException {
			return fromUnmarshaled( aDataStructure );
		}

		/**
		 * This method creates / retrieves (converts to) an instance from the
		 * provided external representation.
		 *
		 * @param aDataStructure The data structure to be marshaled.
		 * 
		 * @return The according marshaled data structure from the external
		 *         representation.
		 * 
		 * @throws MarshalException Thrown when marshaling / serializing a data
		 *         structure fails.
		 */
		C fromUnmarshaled( T aDataStructure ) throws MarshalException;
	}

	/**
	 * The {@link MarshalTypeFactoryComposite} provides
	 * {@link MarshalTypeFactory} as well as
	 * {@link MarshalTypeFactoryComplement} functionality.
	 *
	 * @param <T> The type of the data structure from which a marshaled
	 *        (external) representation is to be unmarshaled.
	 * @param <SRC> The type of the marshaled (external) representation.
	 * @param <C> The type of the complement regarding the type of the marshaled
	 *        (external) representation of the {@link MarshalTypeFactory}.
	 */
	public interface MarshalTypeFactoryComposite<T, SRC, C> extends MarshalTypeFactory<T, SRC>, MarshalTypeFactoryComplement<T, C> {}
}
