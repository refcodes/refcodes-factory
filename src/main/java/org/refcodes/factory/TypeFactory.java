// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.util.Map;

import org.refcodes.mixin.TypeAccessor;

/**
 * The {@link TypeFactory} defines the functionality which must be provided in
 * order to represent a factory for object creation of a predefined type
 * specified with a generic argument (in contrast to the
 * {@link BeanLookupFactory} , which creates instances of an expected type).
 * Many alternative implementations of a {@link TypeFactory} may may exist which
 * construct the instances their way.
 * <p>
 * Having factories that generic as we define it here, we are able to decouple
 * our business logic from any specific framework: Your business logic must not
 * know anything about how the instances are generated. It mainly just needs to
 * know how to use the {@link TypeFactory}. It is up to the application "end
 * point", i.e. a command line tool with a main-method or a web-application to
 * finally decide which factory to use.
 * <p>
 * Depending on the implementation used or configuration provided, the
 * {@link TypeFactory} may return singletons or dedicated separate instances
 * when queried for instances.
 *
 * @param <T> The type of the instances being served by the factory.
 */
@FunctionalInterface
public interface TypeFactory<T> extends TypeAccessor<T>, Factory<T> {

	/**
	 * This method creates / retrieves an instance of the given type.
	 *
	 * @param aProperties The properties which are used to configure the
	 *        fabricated instance (by default the properties are ignored and
	 *        {@link #create()} is called directly).
	 * 
	 * @return The instance being fabricated by this factory.
	 */
	default T create( Map<String, String> aProperties ) {
		return create();
	}

	/**
	 * This method retrieves the type which the implementing factory produces.
	 * 
	 * Attention: As of shortcomings of java'stype system, by default the
	 * {@link Object#getClass()} of the {@link #create()} result is returned (as
	 * we cannot get a generic type's class if not explicitly passed to an
	 * instance e.g. through it's constructor). To avoid unnecessary calls to
	 * potentially expensive {@link #create()} methods, please overwrite this
	 * method!
	 * 
	 * @return The type of the instances this factory produces.
	 */
	@Override
	@SuppressWarnings("unchecked")
	default Class<T> getType() {
		return (Class<T>) create().getClass();
	}
}
