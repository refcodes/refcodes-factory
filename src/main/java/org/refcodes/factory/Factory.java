// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

/**
 * The {@link Factory} defines the most basic functionality which must be
 * provided in order to create objects of a predefined type.
 * 
 * @param <T> The type of the instances being served by the {@link Factory}.
 */
@FunctionalInterface
public interface Factory<T> {

	/**
	 * This method creates / retrieves an instance of the given type.
	 *
	 * @return The instance being fabricated by this factory.
	 */
	T create();

}
