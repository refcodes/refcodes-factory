// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/**
 * The {@link PrototypeFactory} creates instances of a specific type from a
 * prototype instance. Your type must implement either the {@link Serializable}
 * or the {@link Cloneable} interface. Use {@link Cloneable} for a shallow copy
 * by default, and {@link Serializable} for a deep copy by default.
 */
public class PrototypeFactory<T> implements TypeFactory<T>, Serializable {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final T _prototype;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the factory with the according type. Your type must implement
	 * either the {@link Serializable} or the {@link Cloneable} interface. Use
	 * {@link Cloneable} for a shallow copy by default, and {@link Serializable}
	 * for a deep copy by default.
	 * 
	 * @param aPrototype The prototype to be used as prototype by this factory.
	 */
	public PrototypeFactory( T aPrototype ) {
		if ( !( aPrototype instanceof Serializable || aPrototype instanceof Cloneable ) ) {
			throw new IllegalArgumentException( "Your type <" + aPrototype.getClass().getName() + "> must either implement the <Serializable> (defaults to a deep copy) or the <Cloneable> (defaults to a shallow copy interface." );
		}
		_prototype = aPrototype;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T create() {
		IllegalStateException theException = null;
		if ( _prototype instanceof Cloneable ) {
			try {
				return (T) _prototype.getClass().getMethod( "clone" ).invoke( _prototype );
			}
			catch ( IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e ) {
				theException = new IllegalStateException( "Prototype of type <" + getType() + "> is un-cloneable!", e );
			}
		}
		if ( _prototype instanceof Serializable ) {
			try {
				final ByteArrayOutputStream theByteArrayOut = new ByteArrayOutputStream();
				final ObjectOutputStream theObjOut = new ObjectOutputStream( theByteArrayOut );
				theObjOut.writeObject( _prototype );
				final byte[] bytes = theByteArrayOut.toByteArray();
				final ObjectInputStream theObjIn = new ObjectInputStream( new ByteArrayInputStream( bytes ) );
				return (T) theObjIn.readObject();
			}
			catch ( IOException | ClassNotFoundException e ) {
				throw new IllegalStateException( "Prototype of type <" + getType() + "> is un-cloneable!", e );
			}
		}

		throw theException != null ? theException : new IllegalStateException( "Prototype of type <" + getType() + "> is un-cloneable!" );
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<T> getType() {
		return (Class<T>) _prototype.getClass();
	}
}
