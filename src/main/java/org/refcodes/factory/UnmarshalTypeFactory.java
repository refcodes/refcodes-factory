// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.io.InputStream;
import java.util.Map;

import org.refcodes.exception.UnmarshalException;

/**
 * The {@link UnmarshalTypeFactory} defines the functionality which must be
 * provided in order to create unmarshaling functionality from an external
 * representation such as a {@link String} or an {@link InputStream} to an
 * internal representation of a given data structure.
 * 
 * @param <T> The type of the data structure to which an marshaled (external)
 *        representation is to be unmarshaled.
 * @param <SRC> The type of the marshaled representation.
 */
@FunctionalInterface
public interface UnmarshalTypeFactory<T, SRC> {

	/**
	 * This method creates / retrieves (converts to) an instance from the
	 * provided external representation.
	 *
	 * @param aExternalRepresentation The external representation of the data
	 *        structure to be created.
	 * 
	 * @return The according unmarshaled data structure from the external
	 *         representation.
	 * 
	 * @throws UnmarshalException Thrown when unmarshaling / deserializing an
	 *         object fails.
	 */
	T toUnmarshaled( SRC aExternalRepresentation ) throws UnmarshalException;

	/**
	 * This method creates / retrieves (converts to) an instance from the
	 * provided external representation.
	 *
	 * @param aExternalRepresentation The external representation of the data
	 *        structure to be created.
	 * @param aProperties The dynamic properties which are used to configure the
	 *        desired result.
	 * 
	 * @return The according unmarshaled data structure from the external
	 *         representation.
	 * 
	 * @throws UnmarshalException Thrown when unmarshaling / deserializing an
	 *         object fails.
	 */
	default T toUnmarshaled( SRC aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		return toUnmarshaled( aExternalRepresentation );
	}

	/**
	 * The {@link UnmarshalTypeFactoryComplement} provides inverse functionality
	 * to the {@link UnmarshalTypeFactory}.
	 *
	 * @param <T> The type of the data structure to which an external
	 *        representation is to be unmarshaled.
	 * @param <C> The type of the complement regarding the type of the marshaled
	 *        (external) representation of the {@link UnmarshalTypeFactory}.
	 */
	public interface UnmarshalTypeFactoryComplement<T, C> {

		/**
		 * This method creates / retrieves (converts to) an instance from the
		 * provided external representation.
		 *
		 * @param aExternalRepresentation The external representation of the
		 *        data structure to be created.
		 * 
		 * @return The according unmarshaled data structure from the external
		 *         representation.
		 * 
		 * @throws UnmarshalException Thrown when unmarshaling / deserializing
		 *         an object fails.
		 */
		T fromMarshaled( C aExternalRepresentation ) throws UnmarshalException;

		/**
		 * This method creates / retrieves (converts to) an instance from the
		 * provided external representation.
		 *
		 * @param aExternalRepresentation The external representation of the
		 *        data structure to be created.
		 * @param aProperties The dynamic properties which are used to configure
		 *        the desired result.
		 * 
		 * @return The according unmarshaled data structure from the external
		 *         representation.
		 * 
		 * @throws UnmarshalException Thrown when unmarshaling / deserializing
		 *         an object fails.
		 */
		default T fromMarshaled( C aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
			return fromMarshaled( aExternalRepresentation );
		}
	}

	/**
	 * The {@link UnmarshalTypeFactoryComposite} provides
	 * {@link UnmarshalTypeFactory} as well as
	 * {@link UnmarshalTypeFactoryComplement} functionality.
	 *
	 * @param <T> The type of the data structure to which an external
	 *        representation is to be unmarshaled.
	 * @param <SRC> The type of the marshaled (external) representation of the
	 *        {@link UnmarshalTypeFactory}.
	 * @param <C> The type of the complement regarding the type of the marshaled
	 *        (external) representation of the {@link UnmarshalTypeFactory}.
	 */
	public interface UnmarshalTypeFactoryComposite<T, SRC, C> extends UnmarshalTypeFactory<T, SRC>, UnmarshalTypeFactoryComplement<T, C> {}

}
