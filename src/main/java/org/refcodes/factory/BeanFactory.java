// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.util.Set;

/**
 * A lookup factory supporting type and TID lookup.
 * 
 * @param <TID> The type of the IDto be used when looking up an instance.
 */
public interface BeanFactory<TID> extends TypeLookupFactory, BeanLookupFactory<TID> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	<T> T create( TID aIdentifier );

	/**
	 * {@inheritDoc}
	 */
	@Override
	<T> Set<T> create( Class<?> aType );

}
