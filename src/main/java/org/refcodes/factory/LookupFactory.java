// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.util.Map;

/**
 * The {@link LookupFactory} defines the functionality which must be provided in
 * order to represent a factory for object creation of objects identified by an
 * TID {@link String} and of a predefined type specified with a generic argument
 * (in contrast to the {@link BeanLookupFactory}, which creates instances of an
 * expected type). Many alternative implementations of a {@link LookupFactory}
 * may may exist which construct the instances their way.
 * <p>
 * Having factories that generic as we define it here, we are able to decouple
 * our business logic from any specific framework: Your business logic must not
 * know anything about how the instances are generated. It mainly just needs to
 * know how to use the {@link LookupFactory}. It is up to the application "end
 * point", i.e. a command line tool with a main-method or a web-application to
 * finally decide which factory to use.
 * <p>
 * Depending on the implementation used or configuration provided, the
 * {@link LookupFactory} may return singletons or dedicated separate instances
 * when queried for instances.
 *
 * @param <T> The expected type produced by the factory.
 * @param <TID> The type of the TID being used.
 */
@FunctionalInterface
public interface LookupFactory<T, TID> {

	/**
	 * This method creates / retrieves an instance of the configured type
	 * identified with the given identifier (name).
	 * 
	 * @param aIdentifier The name identifying the instance to be created /
	 *        retrieved.
	 * 
	 * @return The instance being fabricated by this factory for the given
	 *         instance name (identifier).
	 */
	T create( TID aIdentifier );

	/**
	 * This method creates / retrieves an instance of the configured type
	 * identified with the given identifier (name).
	 * 
	 * @param aIdentifier The name identifying the instance to be created /
	 *        retrieved.
	 * @param aProperties The dynamic properties which are used to configure the
	 *        desired bean.
	 * 
	 * @return The instance being fabricated by this factory for the given
	 *         instance name (identifier).
	 */
	default T create( TID aIdentifier, Map<String, String> aProperties ) {
		return create( aIdentifier );
	}
}
