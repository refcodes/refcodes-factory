// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * The {@link ClassTypeFactory} produces instances of the given {@link Class} by
 * invoking the empty constructor. Make sure your provided {@link Class} has an
 * empty constructor.
 * 
 * @param <T> The type of the instances being served by the factory.
 */
public class ClassTypeFactory<T> implements TypeFactory<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Class<T> _class;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link ClassTypeFactory} for the given {@link Class}.
	 * 
	 * @param aClass The {@link Class} for which to produce instances.
	 */
	public ClassTypeFactory( Class<T> aClass ) {
		if ( !hasEmptyConstructor( aClass ) ) {
			throw new NoSuchMethodError( "The given class <" + aClass.getName() + "> has no empty constructor, cannot create instances from it!" );
		}
		if ( aClass.isInterface() ) {
			throw new IllegalArgumentException( "The given interface <" + aClass.getName() + "> cannot be used by this factory as no instances can be created from an interface (please pass a class type)!" );
		}
		_class = aClass;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T create() {
		try {
			return _class.getDeclaredConstructor().newInstance();
		}
		catch ( InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e ) {
			throw new InstantiationRuntimeException( "Cannot create an instance for class <" + _class.getName() + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _class;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static boolean hasEmptyConstructor( Class<?> aClass ) {
		for ( Constructor<?> constructor : aClass.getConstructors() ) {
			if ( constructor.getParameterCount() == 0 ) {
				return true;
			}
		}
		return false;
	}
}
