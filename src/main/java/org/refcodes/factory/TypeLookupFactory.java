// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import java.util.Map;
import java.util.Set;

/**
 * The {@link TypeLookupFactory} defines the functionality which must be
 * provided in order to represent a factory for object creation of *ALL* objects
 * being of a requested type (in contrast to the {@link TypeFactory} and the
 * {@link LookupFactory} which create instances of a predefined type specified
 * with a generic argument). Many alternative implementations of a
 * {@link TypeLookupFactory} may may exist which construct the instances their
 * way.
 * <p>
 * Having factories that generic as we define it here, we are able to decouple
 * our business logic from any specific framework: Your business logic must not
 * know anything about how the instances are generated. It mainly just needs to
 * know how to use the {@link TypeLookupFactory}. It is up to the application
 * "end point", i.e. a command line tool with a main-method or a web-application
 * to finally decide which factory to use.
 * <p>
 * Depending on the implementation used or configuration provided, the
 * {@link TypeLookupFactory} may return singletons or dedicated separate
 * instances when queried for instances.
 */
@FunctionalInterface
public interface TypeLookupFactory {

	/**
	 * This method creates / retrieves an all instances of the given type.
	 * 
	 * @param <T> The type which is to be fabricated by the factory method.
	 * @param aType The type identifying the instances to be created /
	 *        retrieved.
	 * 
	 * @return A set with all instance of the required type for the given type.
	 * 
	 * @throws ClassCastException in case the fabricated type is not type
	 *         compatible with the required type T.
	 */
	<T> Set<T> create( Class<?> aType );

	/**
	 * This method creates / retrieves all instances of the given type.
	 * 
	 * @param <T> The type which is to be fabricated by the factory method.
	 * @param aType The type identifying the instances to be created /
	 *        retrieved.
	 * @param aProperties The dynamic properties which are used to configure the
	 *        desired bean.
	 * 
	 * @return A set with all instance of the required type for the given type.
	 * 
	 * @throws ClassCastException in case the fabricated type is not type
	 *         compatible with the required type T.
	 */
	default <T> Set<T> toInstances( Class<?> aType, Map<String, String> aProperties ) {
		return create( aType );
	}
}
