// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

/**
 * The {@link Packager} interface defines methods to pack instances and return
 * the packed, wrapped or boxed instances. Any data being packed with
 * {@link #toPackaged(Object)} must be unpackable with the according counterpart
 * method {@link Extractor#toExtracted(Object)} without loss of information.
 * 
 * @param <UNPACK> The type of the unpacked data.
 * @param <PACK> The type of the packed data.
 * @param <PACKEXC>The exception being thrown when packaging failed.
 */
public interface Packager<UNPACK, PACK, PACKEXC extends Exception> {

	/**
	 * Packs the provided instance, e.g. it is wrapped or boxed with additional
	 * information.
	 * 
	 * @param aUnpacked The data to be packed (enriched).
	 * 
	 * @return The packed data.
	 * 
	 * @throws PACKEXC thrown in case packing the data failed.
	 */
	PACK toPackaged( UNPACK aUnpacked ) throws PACKEXC;

	/**
	 * The {@link Extractor} interface defines methods to unpack instances and
	 * return the unpacked, unwrapped or unboxed instances. Any data being
	 * packed with {@link Packager#toPackaged(Object)} must be unpackable with
	 * the according counterpart method {@link #toExtracted(Object)} without
	 * loss of information.
	 * 
	 * @param <UNPACK> The type of the unpacked data.
	 * @param <PACK> The type of the packed data.
	 * @param <UNPACKEXC>The exception being thrown when unpacking failed.
	 */
	public interface Extractor<PACK, UNPACK, UNPACKEXC extends Exception> {

		/**
		 * Unpacks an instance packed with the the
		 * {@link Packager#toPackaged(Object)} operation Depending on the
		 * implementation and the return type, as a result a new data instance
		 * is returned or the same instance is being modified (and returned).
		 * 
		 * @param aPacked The packed data to be unpacked.
		 * 
		 * @return The unpacked data.
		 * 
		 * @throws UNPACKEXC thrown in case unpacking the data failed.
		 */
		UNPACK toExtracted( PACK aPacked ) throws UNPACKEXC;

		/**
		 * Method for potential better code expressiveness, the same as
		 * {@link #toExtracted(Object)}.
		 * 
		 * @param aPacked The packed data to be unpacked.
		 * 
		 * @return The unpacked data.
		 * 
		 * @throws UNPACKEXC thrown in case unpacking the data failed.
		 */
		default UNPACK fromPackaged( PACK aPacked ) throws UNPACKEXC {
			return toExtracted( aPacked );
		}
	}

	/**
	 * The {@link PackageProcessor} type combines a {@link Packager} with an
	 * {@link Extractor} to package and extract in one module.
	 * 
	 * @param <UNPACK> The type of the unpacked data.
	 * @param <PACK> The type of the packed data.
	 * @param <PACKEXC>The exception being thrown when packaging failed.
	 * @param <UNPACKEXC>The exception being thrown when unpacking failed.
	 */
	public interface PackageProcessor<UNPACK, PACK, UNPACKEXC extends Exception, PACKEXC extends Exception> extends Packager<PACK, UNPACK, UNPACKEXC>, Extractor<UNPACK, PACK, PACKEXC> {}
}
