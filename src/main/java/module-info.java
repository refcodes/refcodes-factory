module org.refcodes.factory {
	requires transitive org.refcodes.exception;
	requires org.refcodes.mixin;

	exports org.refcodes.factory;
}
