// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory;

import static org.junit.jupiter.api.Assertions.*;
import java.io.Serializable;
import org.junit.jupiter.api.Test;

public class PrototypeFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPrototypeFactory1() {
		final Person thePrototype = new Person( "John", "Romero", new Person( "John", "Carmack" ) );
		final PrototypeFactory<Person> theFactory = new PrototypeFactory<>( thePrototype );
		final Person thePerson = theFactory.create();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Prototype: " + thePrototype.toString() );
			System.out.println( "   Person: " + thePerson.toString() );
		}
		assertNotSame( thePrototype, thePerson );
		assertEquals( thePrototype, thePerson );
	}

	@Test
	public void testPrototypeFactory2() {
		final Buddy thePrototype = new Buddy( "John", "Romero", new Buddy( "John", "Carmack" ) );
		final PrototypeFactory<Buddy> theFactory = new PrototypeFactory<>( thePrototype );
		final Buddy theBuddy = theFactory.create();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Prototype: " + thePrototype.toString() );
			System.out.println( "    Buddy: " + theBuddy.toString() );
		}
		assertNotSame( thePrototype, theBuddy );
		assertEquals( thePrototype, theBuddy );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person implements Cloneable {
		private String firstName;
		private String lastName;
		private Person person;

		/**
		 * Instantiates a new buddy.
		 */
		public Person() {};

		/**
		 * Instantiates a new buddy.
		 *
		 * @param firstName the first name
		 * @param lastName the last name
		 */
		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		/**
		 * Instantiates a new buddy.
		 *
		 * @param firstName the first name
		 * @param lastName the last name
		 */
		public Person( String firstName, String lastName, Person person ) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.person = person;
		}

		/**
		 * Gets the first name.
		 *
		 * @return the first name
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * Sets the first name.
		 *
		 * @param firstName the new first name
		 */
		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		/**
		 * Gets the last name.
		 *
		 * @return the last name
		 */
		public String getLastName() {
			return lastName;
		}

		/**
		 * Sets the last name.
		 *
		 * @param lastName the new last name
		 */
		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}

		public Person getPerson() {
			return person;
		}

		public void setPerson( Person person ) {
			this.person = person;
		}

		@Override
		public Object clone() throws CloneNotSupportedException {
			return super.clone();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( firstName == null ) ? 0 : firstName.hashCode() );
			result = prime * result + ( ( lastName == null ) ? 0 : lastName.hashCode() );
			result = prime * result + ( ( person == null ) ? 0 : person.hashCode() );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Person other = (Person) obj;
			if ( firstName == null ) {
				if ( other.firstName != null ) {
					return false;
				}
			}
			else if ( !firstName.equals( other.firstName ) ) {
				return false;
			}
			if ( lastName == null ) {
				if ( other.lastName != null ) {
					return false;
				}
			}
			else if ( !lastName.equals( other.lastName ) ) {
				return false;
			}
			if ( person == null ) {
				if ( other.person != null ) {
					return false;
				}
			}
			else if ( !person.equals( other.person ) ) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + ", buddy=" + person + "]";
		}
	}

	public static class Buddy implements Serializable {
		private static final long serialVersionUID = 1L;
		private String firstName;
		private String lastName;
		private Buddy buddy;

		/**
		 * Instantiates a new buddy.
		 */
		public Buddy() {};

		/**
		 * Instantiates a new buddy.
		 *
		 * @param firstName the first name
		 * @param lastName the last name
		 */
		public Buddy( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		/**
		 * Instantiates a new buddy.
		 *
		 * @param firstName the first name
		 * @param lastName the last name
		 */
		public Buddy( String firstName, String lastName, Buddy buddy ) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.buddy = buddy;
		}

		/**
		 * Gets the first name.
		 *
		 * @return the first name
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * Sets the first name.
		 *
		 * @param firstName the new first name
		 */
		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		/**
		 * Gets the last name.
		 *
		 * @return the last name
		 */
		public String getLastName() {
			return lastName;
		}

		/**
		 * Sets the last name.
		 *
		 * @param lastName the new last name
		 */
		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}

		public Buddy getBuddy() {
			return buddy;
		}

		public void setBuddy( Buddy buddy ) {
			this.buddy = buddy;
		}

		@Override
		public Object clone() throws CloneNotSupportedException {
			return super.clone();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( firstName == null ) ? 0 : firstName.hashCode() );
			result = prime * result + ( ( lastName == null ) ? 0 : lastName.hashCode() );
			result = prime * result + ( ( buddy == null ) ? 0 : buddy.hashCode() );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Buddy other = (Buddy) obj;
			if ( firstName == null ) {
				if ( other.firstName != null ) {
					return false;
				}
			}
			else if ( !firstName.equals( other.firstName ) ) {
				return false;
			}
			if ( lastName == null ) {
				if ( other.lastName != null ) {
					return false;
				}
			}
			else if ( !lastName.equals( other.lastName ) ) {
				return false;
			}
			if ( buddy == null ) {
				if ( other.buddy != null ) {
					return false;
				}
			}
			else if ( !buddy.equals( other.buddy ) ) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return "Buddy [firstName=" + firstName + ", lastName=" + lastName + ", buddy=" + buddy + "]";
		}
	}
}
